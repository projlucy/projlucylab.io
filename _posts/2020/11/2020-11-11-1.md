---
layout: post
title: "Application release plan"
---

NetKiller, NetKillerW and TopNViewer will be released in November 30, 2020.

|Application|Requirements|
|---|---|
|NetKiller|Rooting android phone|
|NetKillerW|Rooting android phone with internal monitor mode adapter(Nexus 5)|
|TopNViewer|Rooting android phone with internal monitor mode adapter(Nexus 5)|

If any question, feel free to send an email( gilgi19793@gmail.com ). Thank you.
